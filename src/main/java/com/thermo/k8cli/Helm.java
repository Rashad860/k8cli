package com.thermo.k8cli;

import com.beust.jcommander.Parameter;
import com.thermo.k8cli.k8.K8Base;

import java.io.IOException;

public class Helm extends K8Base {

    @Parameter(names = "-image_url", description = "Image to Repository.")
    public String image_url;

    public void docker_login() throws IOException, InterruptedException {
        Shell.execute(new String[]{"docker", "login", "-u", docker_username, "-p",
                docker_password, image_url});
    }

    public void install() throws IOException, InterruptedException {
        docker_login();
        helm_init();
        Shell.execute(new String[]{"helm", "registry", "install", image_url,
                "--namespace", namespace});
    }

    public void helm_init() throws IOException, InterruptedException {
        Shell.execute(new String[]{"helm", "init"});
    }

}
