package com.thermo.k8cli;

import com.beust.jcommander.JCommander;
import com.thermo.k8cli.k8.K8Base;
import com.thermo.k8cli.k8.Namespace;
import com.thermo.k8cli.k8.Secret;

import java.io.IOException;

public class App
{
    public static void main( String[] args ) throws IOException, InterruptedException {
        K8Base envArgs = new K8Base();
        Namespace cc = new Namespace();
        Secret cs = new Secret();
        Helm hm = new Helm();
        JCommander jc = JCommander.newBuilder()
                .addObject(envArgs)
                .addCommand("create-namespace", cc)
                .addCommand("create-secret", cs)
                .addCommand("install", hm)
                .build();

//        jc.parse("create-namespace", "-cluster", "C:\\Users\\rashad.colebrooke.AMER\\.kube\\config", "-namespace", "tester");
//        jc.parse("create-secret", "-cluster", "C:\\Users\\rashad.colebrooke.AMER\\.kube\\config", "-namespace", "default", "-name", "test-secret",
//                "-docker_username", "slakowske", "-docker_password", "ac83254a-9022-11e8-a93e-784f4358664b", "-docker_email", "slakowske@coreinform", "-repository_host" ,"quay.coredev.cloud");
//        jc.parse("install", "-docker_username", "slakowske", "-docker_password", "ac83254a-9022-11e8-a93e-784f4358664b", "-namespace", "default", "-image_url", "quay.coredev.cloud/core-informatics/cjd-test-chart@0.1.11", "-repository_host", "quay.coredev.cloud");

        jc.parse(args);

        switch (jc.getParsedCommand()){
            case "create-namespace":
                cc.create();
                break;
            case "create-secret":
                cs.create();
                break;
            case "install":
                hm.install();
                break;
        }
    }
}
