package com.thermo.k8cli.k8;

import com.beust.jcommander.Parameter;
import com.squareup.okhttp.Response;
import io.kubernetes.client.ApiException;
import io.kubernetes.client.models.V1Secret;
import io.kubernetes.client.models.V1SecretBuilder;
import io.kubernetes.client.util.Yaml;
import net.minidev.json.JSONObject;

import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class Secret extends K8Base {
    @Parameter(names = "-name", description = "Secret Identification.")
    public String name;

    public void create() throws IOException {

        JSONObject details = new JSONObject();
        JSONObject dockerMap = new JSONObject();

        String dockerStringCredential = docker_username + ":" + docker_password;
        String credentials = Base64.getEncoder().encodeToString(dockerStringCredential.getBytes());

        details.put("username", docker_username);
        details.put("password", docker_password);
        details.put("email", docker_email);
        details.put("auth", credentials);
        dockerMap.put(repository_host,details.toJSONString());
        String dockerConfigString = dockerMap.toString();

        Map<String, byte[]> data = new HashMap();
        data.put(".dockercfg",dockerConfigString.getBytes());

        V1Secret secret = new V1SecretBuilder()
                .withNewMetadata()
                .withName(name)
                .endMetadata()
                .withApiVersion("v1")
                .withKind("Secret")
                .withType("kubernetes.io/dockercfg")
                .withData(data)
                .build();
        System.out.println(Yaml.dump(secret));
        Response response = null;
        try {
            response = getApi().createNamespacedSecretCall(namespace,secret, "pretty", null, null).execute();
        } catch (ApiException e) {
            e.printStackTrace();
        }
        System.out.println(response);
    }
}
