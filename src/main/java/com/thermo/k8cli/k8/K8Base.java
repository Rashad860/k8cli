package com.thermo.k8cli.k8;
import com.beust.jcommander.Parameter;
import io.kubernetes.client.apis.CoreV1Api;
import java.io.FileReader;
import java.io.IOException;

public class K8Base {
    @Parameter(names = "-cluster", description = "Kubernetes Config file.")
    public String cluster;

    @Parameter(names = "-namespace", description = "Namespace Identification.")
    public String namespace;

    @Parameter(names = "-docker_username", description = "Username for Docker.")
    public String docker_username;

    @Parameter(names = "-docker_password", description = "Password for Docker.")
    public String docker_password;

    @Parameter(names = "-docker_email", description = "Email for Docker.")
    public String docker_email;

    @Parameter(names = "-repository_host", description = "Repository Host.")
    public String repository_host;

    public CoreV1Api getApi() throws IOException {
        CoreV1Api api = new Client(new FileReader(cluster)).get();
        return api;
    }
}
