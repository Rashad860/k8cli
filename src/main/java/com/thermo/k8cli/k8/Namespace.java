package com.thermo.k8cli.k8;

import com.squareup.okhttp.Response;
import io.kubernetes.client.ApiException;
import io.kubernetes.client.models.V1Namespace;
import io.kubernetes.client.models.V1NamespaceBuilder;
import io.kubernetes.client.util.Yaml;

import java.io.IOException;

public class Namespace extends K8Base {
    public void create() throws IOException {
        V1Namespace ns = new V1NamespaceBuilder()
                .withApiVersion("v1")
                .withKind("Namespace")
                .withNewMetadata()
                .withName(namespace)
                .endMetadata()
                .build();
        System.out.println(Yaml.dump(ns));
        Response response = null;
        try {
            response = getApi().createNamespaceCall(ns, "pretty", null, null).execute();
        } catch (ApiException e) {
            e.printStackTrace();
        }
        System.out.println(response);
    }
}
