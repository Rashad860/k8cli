package com.thermo.k8cli.k8;

import io.kubernetes.client.ApiClient;
import io.kubernetes.client.Configuration;
import io.kubernetes.client.apis.CoreV1Api;
import io.kubernetes.client.util.Config;

import java.io.FileReader;
import java.io.IOException;

public class Client {
    private CoreV1Api api;

    public Client(FileReader reader) throws IOException {
        ApiClient apiClient = Config.fromConfig(reader);
        Configuration.setDefaultApiClient(apiClient);
        api = new CoreV1Api();
    }

    public CoreV1Api get() {
        return api;
    }
}
